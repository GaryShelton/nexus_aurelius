object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 494
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  DesignSize = (
    657
    494)
  PixelsPerInch = 96
  TextHeight = 13
  object DBNavigator1: TDBNavigator
    Left = 8
    Top = 8
    Width = 360
    Height = 41
    DataSource = DataSource1
    TabOrder = 0
  end
  object DBGrid1: TDBGrid
    Left = 8
    Top = 72
    Width = 641
    Height = 414
    Anchors = [akLeft, akTop, akRight, akBottom]
    DataSource = DataSource1
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = []
  end
  object Button1: TButton
    Left = 392
    Top = 24
    Width = 97
    Height = 25
    Caption = 'Connect Native'
    TabOrder = 2
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 504
    Top = 24
    Width = 75
    Height = 25
    Caption = 'Aurelius'
    TabOrder = 3
    OnClick = Button2Click
  end
  object DataSource1: TDataSource
    Left = 120
    Top = 128
  end
  object nxSession1: TnxSession
    ServerEngine = nxServerEngine1
    Left = 128
    Top = 272
  end
  object nxseAllEngines1: TnxseAllEngines
    Left = 32
    Top = 208
  end
  object nxDatabase1: TnxDatabase
    Session = nxSession1
    AliasPath = '.'
    Left = 128
    Top = 336
  end
  object nxServerEngine1: TnxServerEngine
    SqlEngine = nxSqlEngine1
    ServerName = ''
    Options = []
    TableExtension = 'nx1'
    Left = 128
    Top = 208
  end
  object nxTable1: TnxTable
    Database = nxDatabase1
    TableName = 'employees'
    Exclusive = True
    Left = 128
    Top = 400
  end
  object AureliusDataset1: TAureliusDataset
    FieldDefs = <>
    CreateSelfField = False
    Left = 368
    Top = 216
    object AureliusDataset1ID: TIntegerField
      FieldName = 'ID'
    end
    object AureliusDataset1FirstName: TStringField
      DisplayWidth = 15
      FieldName = 'FirstName'
      Size = 50
    end
    object AureliusDataset1LastName: TStringField
      DisplayWidth = 25
      FieldName = 'LastName'
      Size = 50
    end
    object AureliusDataset1HireDate: TDateField
      FieldName = 'HireDate'
    end
    object AureliusDataset1TerminateDate: TDateField
      FieldName = 'TerminateDate'
    end
    object AureliusDataset1PayRate: TCurrencyField
      FieldName = 'PayRate'
    end
  end
  object AureliusConnection1: TAureliusConnection
    AdapterName = 'NexusDB'
    AdaptedConnection = nxDatabase1
    SQLDialect = 'NexusDB'
    Left = 368
    Top = 168
  end
  object nxSqlEngine1: TnxSqlEngine
    StmtLogging = False
    StmtLogTableName = 'QueryLog'
    UseFieldCache = False
    Left = 48
    Top = 280
  end
end
