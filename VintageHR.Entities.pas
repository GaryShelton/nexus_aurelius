unit VintageHR.Entities;

interface

uses
  Aurelius.Mapping.Attributes
  ,Aurelius.Types.Nullable
  ,System.Classes
  ;

type
  [Entity]
  [Table('employees')]
  //[Automapping]
  [Id('FID', TIdGenerator.IdentityOrSequence)]
  TEmployee = class
    private
      [Column('ID',[TColumnProp.Required])]
      FID: Integer;
      [Column('FirstName',[TcolumnProp.Required], 50)]
      FFirstName: String;
      [Column('LastName',[TcolumnProp.Required], 50)]
      FLastName: String;
      [Column('HireDate',[TColumnProp.Required])]
      FHireDate: TDate;
      [Column('TerminateDate')]
      FTerminateDate: Nullable<TDate>;
      [Column('PayRate')]
      FPayRate: Nullable<Currency>;
    public
      property ID: Integer read FID write FID;
      property FirstName: String read FFirstName write FFirstName;
      property LastName: String read FLastName write FLastName;
      property HireDate: TDate read FHireDate write FHireDate;
      property TerminateDate: Nullable<TDate> read FTerminateDate write FTerminateDate;
      property PayRate: Nullable<Currency> read FPayRate write FPayRate;
  end;

//  TMissedDaysReason = (mdSick, mdVacation, mdPersonal);
//  TMissedDaysReasons = set of TMissedDaysReason;
//
//  [Entity, AutoMapping]
//  [Table('MissadDays')]
//  TMissedDay = class
//    private
//      FID: Integer;
//      FEmployee: TEmployee;
//      FStartDate: TDate;
//      FStopDate: Nullable<TDate>;
//      FReason: TMissedDaysReason;
//    public
//      property ID: Integer read FID write FID;
//      property Employee: TEmployee read FEmployee write FEmployee;
//      property StartDate: TDate read FStartDate write FStartDate;
//      property StopDate: Nullable<TDate> read FStopDate write FStopDate;
//      property Reason: TMissedDaysReason read FReason write FReason;
//  end;
//
//  [Entity, Automapping]
//  [Table('Tracking')]
//  TTracking = class
//    private
//      FID: Integer;
//      FEmployee: TEmployee;
//      FOfferedMedical: Boolean;
//      FAcceptedMedical: Boolean;
//      FMedicalStartDate: Nullable<TDate>;
//      FOfferedIRA: Boolean;
//      FAddedToAuto: Boolean;
//      FAutoDateRequested: Nullable<TDate>;
//      FAddedToPayroll: Boolean;
//      FInitialPaperworkComplete: Boolean;
//      FNotes: TStrings;
//    public
//      constructor Create;
//      property ID: Integer read FID write FID;
//      property Employee: TEmployee read FEmployee write FEmployee;
//  end;
//
//  [Entity, Automapping]
//  [Table('PayHistory')]
//  TPayHistory = class
//    private
//      FID: Integer;
//      FEmployee: TEmployee;
//      FRaiseDate: TDate;
//      FNewPayRate: Currency;
//    public
//      property ID: Integer read FID write FID;
//      property Employee: TEmployee read FEmployee write FEmployee;
//      property RaiseDate: TDate read FRaiseDate write FRaiseDate;
//      property NewPayRate: Currency read FNewPayRate write FNewPayRate;
//  end;


implementation

{ TTracking }

//constructor TTracking.Create;
//begin
//  FOfferedMedical:= False;
//  FAcceptedMedical:= False;
//  FMedicalStartDate:= nil;
//  FOfferedIRA:= False;
//  FAddedToAuto:= False;
//  FAutoDateRequested:= nil;
//  FAddedToPayroll:= False;
//  FInitialPaperworkComplete := False;
//  FNotes:= Nil;
//end;

initialization
  RegisterEntity(TEmployee);
//  RegisterEntity(TTracking);
//  RegisterEntity(TPayHistory);
//  RegisterEntity(TMissedDay);
end.
