unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants,
  System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.ExtCtrls, Vcl.DBCtrls, nxdb, nxsdServerEngine, nxsrServerEngine,
  nxseAutoComponent, nxllComponent, Vcl.StdCtrls, Aurelius.Sql.NexusDB,
  Aurelius.Schema.NexusDB, Aurelius.Drivers.NexusDB, Aurelius.Comp.Connection,
  Aurelius.Bind.BaseDataset, Aurelius.Bind.Dataset, VintageHR.Entities,
  Aurelius.Drivers.Interfaces, Aurelius.Engine.ObjectManager,
  System.Generics.Collections, nxsrSqlEngineBase, nxsqlEngine;

type
  TForm1 = class(TForm)
    DBNavigator1: TDBNavigator;
    DBGrid1: TDBGrid;
    Button1: TButton;
    DataSource1: TDataSource;
    nxSession1: TnxSession;
    nxseAllEngines1: TnxseAllEngines;
    nxDatabase1: TnxDatabase;
    nxServerEngine1: TnxServerEngine;
    nxTable1: TnxTable;
    Button2: TButton;
    AureliusDataset1: TAureliusDataset;
    AureliusConnection1: TAureliusConnection;
    nxSqlEngine1: TnxSqlEngine;
    AureliusDataset1ID: TIntegerField;
    AureliusDataset1FirstName: TStringField;
    AureliusDataset1LastName: TStringField;
    AureliusDataset1HireDate: TDateField;
    AureliusDataset1TerminateDate: TDateField;
    AureliusDataset1PayRate: TCurrencyField;
    procedure Button1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
    FConnection: IDBConnection;
    ObjectManager: TObjectManager;

  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  DataSource1.Dataset := nxTable1;
  nxServerEngine1.Active := True;
  nxSession1.Active := True;
  nxDatabase1.Active := True;
  nxTable1.Active := True;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  List: TList<TEmployee>;
begin
  DataSource1.Dataset := AureliusDataset1;
  nxServerEngine1.Active := True;
  nxSession1.Active := True;
  nxDatabase1.Active := True;

  FConnection := AureliusConnection1.CreateConnection;
  ObjectManager := TObjectManager.Create(FConnection);
  AureliusDataset1.Manager := ObjectManager;
  List := ObjectManager.Find<TEmployee>.List;
  AureliusDataset1.SetSourceList(List);
  AureliusDataset1.Active := True;
end;

procedure TForm1.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  AureliusDataset1.Active := False;
  nxServerEngine1.Active := False;
end;

end.
